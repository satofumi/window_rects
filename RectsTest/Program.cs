﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RectsTest
{
    class Program
    {
        static void Main(string[] args)
        {
            WindowRects.WindowRects.ClearFilters();
            WindowRects.WindowRects.AddIgnoreClassName("Windows.UI.Core.CoreWindow");
            WindowRects.WindowRects.AddIgnoreClassName("ApplicationFrameWindow");
            //WindowRects.WindowRects.AddClassNameFilter("CabinetWClass");
            //WindowRects.WindowRects.AddClassNameFilter("UnityWndClass");
            var rects = WindowRects.WindowRects.Rects;
            foreach (var rect in rects)
            {
                System.Diagnostics.Debug.WriteLine($"{rect.Name}, {rect.ClassName} {rect.Left}, {rect.Top}, {rect.Width}, {rect.Height}");
            }
        }
    }
}
