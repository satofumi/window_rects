﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace WindowRects
{
    public static class WindowRects
    {
        public class Window
        {
            public string Name { private set; get; }
            public string ClassName { private set; get; }
            public float Left { private set; get; }
            public float Top { private set; get; }
            public float Width { private set; get; }
            public float Height { private set; get; }

            public Window(string name, string className, float left, float top, float width, float height)
            {
                Name = name;
                ClassName = className;
                Left = left;
                Top = top;
                Width = width;
                Height = height;
            }
        }


        private static HashSet<string> findClassNames = new HashSet<string>();
        private static HashSet<string> ignoreClassNames = new HashSet<string>();


        public static void ClearFilters()
        {
            findClassNames.Clear();
            ignoreClassNames.Clear();
        }

        public static void AddFindClassName(string name)
        {
            findClassNames.Add(name);
        }

        public static void AddIgnoreClassName(string name)
        {
            ignoreClassNames.Add(name);
        }

        public static List<Window> Rects
        {
            get
            {
                windowRects.Clear();
                EnumWindows(new EnumWindowsDelegate(EnumWindowCallBack), IntPtr.Zero);

                return windowRects;
            }
        }

        #region
        [DllImport("user32.dll")]
        private static extern bool EnumWindows(EnumWindowsDelegate lpEnumFunc, IntPtr lParam);
        private delegate bool EnumWindowsDelegate(IntPtr hWnd, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetWindowText(IntPtr hWnd, StringBuilder lpString, int nMaxCount);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern int GetClassName(IntPtr hWnd, StringBuilder lpClassName, int nMaxCount);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool IsIconic(IntPtr hWnd);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool IsWindowVisible(IntPtr hWnd);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern bool IsWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        private static extern long GetWindowLong(IntPtr hWnd, int nIndex);

        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        private struct RECT
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        [DllImport("User32.Dll")]
        static extern int GetWindowRect(IntPtr hWnd, out RECT rect);

        
        private const int GWL_EXSTYLE = -20;
        private const long WS_EX_TOOLWINDOW = 0x00000080;
        private static List<Window> windowRects = new List<Window>();

#if UNITY_STANDALONE
        [AOT.MonoPInvokeCallback(typeof(EnumWindowsDelegate))]
#endif
        private static bool EnumWindowCallBack(IntPtr hWnd, IntPtr lparam)
        {
            int textLen = GetWindowTextLength(hWnd);
            if (textLen <= 0)
            {
                return true;
            }

            if (IsIconic(hWnd))
            {
                return true;
            }
            if (!IsWindowVisible(hWnd))
            {
                return true;
            }
            if (!IsWindow(hWnd))
            {
                return true;
            }

            var style = GetWindowLong(hWnd, GWL_EXSTYLE);
            if ((style & WS_EX_TOOLWINDOW) != 0)
            {
                return true;
            }

            var csb = new StringBuilder(32);
            GetClassName(hWnd, csb, csb.Capacity);
            var className = csb.ToString();
            csb = null;
            if (findClassNames.Count > 0)
            {
                if (!findClassNames.Contains(className))
                {
                    return true;
                }
            }
            if (ignoreClassNames.Count > 0)
            {
                if (ignoreClassNames.Contains(className))
                {
                    return true;
                }
            }

            GetWindowRect(hWnd, out RECT rect);
            int width = rect.right - rect.left;
            int height = rect.bottom - rect.top;

            var tsb = new StringBuilder(textLen + 1);
            GetWindowText(hWnd, tsb, tsb.Capacity);
            var windowName = tsb.ToString();
            tsb = null;

            windowRects.Add(new Window(windowName, className, rect.left, rect.top, width, height));

            return true;
        }
        #endregion
    }
}
